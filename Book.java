import java.util.Random;
public class Book{
    static Random rnd = new Random();
    String titolo;
    String autore;
    int pagine;
    static int nlibri = 0;
    public Book(String titolo,String autore,int pagine){
    this.titolo = titolo;
    this.autore = autore;
    this.pagine = pagine;
    nlibri++;
}
    public static void main(String[] args){
      Book[] libreria = new Book[20];
      int cont = 0;
      for(int i = 0; i < libreria.length;i++){
          libreria[i] = new Book(Integer.toString(i),Integer.toString(i),rnd.nextInt(31)+20);
      }
      for(int i = 0; i < libreria.length; i++){
          if(libreria[i].pagine >= 30){
              cont++;
              System.out.print(libreria[i].titolo + " ");
        }
      }
      System.out.println("\nla percentuale dei libri con piu' di 30 pagine e' " + cont*100.0/nlibri);

      Book[] ar = new Book[10];
      for (int i = 0; i < ar.length; i++) {
          ar[i] = factory();
      }
      Book b1 = new Book();
      boolean b = false;
      for (int i = 0; i < ar.length; i++) {
            for (int j = i + 1; j < ar.length; j++) {
                b = false;
                cont = 0;
      while (b == false) {
          if (ar[i].autore.charAt(cont) > ar[j].autore.charAt(cont)) {
              b1 = ar[i];
              ar[i] = ar[j];
              ar[j] = b1;
              b = true;
      }else  if (ar[i].autore.charAt(cont) == ar[j].autore.charAt(cont)){
              cont++;
      }else {
              b = true;
         }
        }
       }
      }
      System.out.println("\ni libri ordinati per autore sono: ");
      for (int i = 0;i < ar.length; i++) {
          System.out.println(ar[i].autore);
      }
      int al = 0;
      for (int i = 0; i < ar.length; i++) {
          for (int j = 0; j < ar.length; j++) {
              if (ar[i].pagine < ar[j].pagine) {
                  al = ar[i].pagine;
                  ar[i].pagine = ar[j].pagine;
                  ar[j].pagine = al;
        }
       }
      }
      System.out.println("\ni libri ordinati per pagine sono: ");
      for (int i = 0; i < ar.length; i++) {
        System.out.println(ar[i].pagine);
     }
    }
    public static Book factory() {
        String aut = "";
        String tit = "";
        int pag = Book.rnd.nextInt(20) + 80;
        for (int i = 0; i < 8; i++) {
            aut += (char)('a' + Book.rnd.nextInt(26));
      }
        for (int i = 0; i < 10; i++) {
          tit += (char)('a' + Book.rnd.nextInt(26));
    }
        Book bo = new Book(tit, aut, pag);
        return bo;
  }
}